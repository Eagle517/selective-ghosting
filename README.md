# SelectiveGhosting
A DLL that allows you to ghost individual objects to clients in Blockland.

Credit to [Zeblote](https://github.com/Zeblote) for the original version.

## Usage
`NetConnection::setNetFlag(int bit, bool flag)` - Sets an object's net flag

`NetConnection::getNetFlag(int bit)` - Gets an object's net flag

`NetConnection::clearScopeAlways()` - Removes an object from the GhostAlwaysSet and kills its ghost for all clients

`NetConnection::setScopeAlways()` - Adds an object to the GhostAlwaysSet and marks it to be ghosted for all clients

`NetConnection::clearScopeToClient(%client)` - Kills an object's ghost for %client

`NetConnection::scopeToClient(%client)` - Ghosts an object to %client

## Example
Let %brick be some fxDTSBrick.
```
%brick.setNetFlag(6, true);
%brick.clearScopeToClient(findClientByName("Eagle517"));
```
Enabling the 6th net flag allows us to control if the object is scoped. We then tell the game to unscope the brick from the client. If we wanted to scope the brick back to the client, we can do `%brick.scopeToClient(findClientByName("Eagle517"));` Alternatively, disabling the 6th net flag will cause the brick to scope to all clients on the next ghost update.
