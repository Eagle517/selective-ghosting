#include <windows.h>

#include "TSFuncs.hpp"

BlFunctionDef(void, __thiscall, NetConnection__detachObject, ADDR, ADDR);

ADDR gNetConnection__GhostLookupTableSize;

/* NetObject::objectLocalClearAlways() got inlined.
   As a solution, I'm just overwriting the NetObject::clearScopeToClient()
   console method. */
void NetObject__clearScopeToClient(ADDR obj, int argc, const char *argv[])
{
	ADDR client = tsf_FindObject(argv[2]);
	if (!client) {
		BlPrintf("\x03NetObject::clearScopeToClient: Couldn't find connection %s", argv[2]);
		return;
	}

	const char *className = *(const char **)((**(int (***)(void))client)() + 32);
	if (strcmp(className, "GameConnection") != 0) {
		BlPrintf("\x03NetObject::clearScopeToClient: Couldn't find connection %s", argv[2]);
		return;
	}

	ADDR conn = client - 160; // NetConnection is at GameConnection - 160
	if (*(ADDR *)(conn + 488)) {
		ADDR ref = *(ADDR *)(*(ADDR *)(conn + 520) + 4*(*(ADDR *)(obj + 32) & (*(ADDR *)gNetConnection__GhostLookupTableSize - 1)));
		for (; ref; ref = *(ADDR *)(ref + 32)) {
			if (*(ADDR *)ref == obj) {
				if (*(ADDR *)(obj + 68) & ~0xFFFFFFBF)
					NetConnection__detachObject(conn, ref);
				else
					*(WORD *)(ref + 10) &= 0xFEFFu;

				break;
			}
		}
	}
}

void NetObject__clearScopeAlways(ADDR obj, int argc, const char *argv[])
{
	if (!(*(ADDR *)(obj + 68) & 2)) {
		*(ADDR *)(obj + 68) &= 0xFFFFFFBF;

		ADDR ghostAlwaysSet = tsf_FindObject("GhostAlwaysSet");
		if (ghostAlwaysSet)
			(*(void (__stdcall **)(int))(*(ADDR *)ghostAlwaysSet + 72))(obj); // SimObjectList::remove(SimObject* obj)

		for (ADDR ref = *(ADDR *)(obj + 76); ref; ref = *(ADDR *)(obj + 76))
			NetConnection__detachObject(*(ADDR *)(ref + 28), ref);
	}
}

void NetObject__setNetFlag(ADDR obj, int argc, const char *argv[])
{
	if (atoi(argv[3]) || stricmp(argv[3], "true") >= 0)
		*(ADDR *)(obj + 68) |= 1 << atoi(argv[2]);
	else
		*(ADDR *)(obj + 68) &= ~(1 << atoi(argv[2]));
}

bool NetObject__getNetFlag(ADDR obj, int argc, const char *argv[])
{
	return (*(ADDR *)(obj + 68) >> atoi(argv[2])) & 1;
}

bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	BlScanFunctionHex(NetConnection__detachObject, "56 8B 74 24 08 57 8B F9 66 83 4E ? ?");
	ADDR BlScanHex(NetConnection__GhostLookupTableSizeLoc, "8B 0D ? ? ? ? 8B 86 ? ? ? ? 49 23 4A 20 8B 04 88 85 C0 74 17");

	gNetConnection__GhostLookupTableSize = *(ADDR *)(NetConnection__GhostLookupTableSizeLoc + 2);

	tsf_AddConsoleFunc(NULL, "NetObject", "clearScopeAlways", NetObject__clearScopeAlways, "NetObject::clearScopeAlways()", 2, 2);
	tsf_AddConsoleFunc(NULL, "NetObject", "clearScopeToClient", NetObject__clearScopeToClient, "clearScopeToClient(%client) Undo the effects of a scopeToClient() call.", 3, 3);
	tsf_AddConsoleFunc(NULL, "NetObject", "setNetFlag", NetObject__setNetFlag, "NetObject::setNetFlag(int bit, bool flag)", 4, 4);
	tsf_AddConsoleFunc(NULL, "NetObject", "getNetFlag", NetObject__getNetFlag, "NetObject::getNetFlag(int bit)", 3, 3);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

bool deinit()
{
	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
